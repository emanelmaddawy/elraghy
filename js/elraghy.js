$(function(){

 $('.owl-carousel#sync1').owlCarousel({
        items:1,
        loop:true,
        nav: true,
        navText: ['<i class="fa fa-angle-left"><i>', '<i class="fa fa-angle-right"><i>'],
        URLhashListener:true,
        autoplayHoverPause:true,
        startPosition: 'URLHash'
    });

    $('.owl-carousel#sync2').owlCarousel({
   loop:true,
   margin:1,
   nav:true,
   mouseDrag:true,
   navText: ['<i class="fa fa-angle-left"></i>', '<i class="fa fa-angle-right"></i>'],
   responsive:{
       0:{
           items:1,
           mouseDrag:false
       },
       600:{
           items:3,
           mouseDrag:true
       },
       1000:{
           items:4
       }
   }
});

});
